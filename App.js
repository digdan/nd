import React from 'react';
import * as Expo from 'expo';
import SetupScreens from './screens/SetupScreens';
import { Ionicons } from '@expo/vector-icons';
import { Provider } from 'react-redux';
import store from './store';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady:false
    };
  }

  componentWillMount() {
    this.loadFonts();
  }

  async loadFonts() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font
    });
    this.setState({
      isReady:true
    });
  }

  render() {
    if (!this.state.isReady) {
      return <Expo.AppLoading />;
    } else {
      return (
        <Provider store={store}>
          <SetupScreens />
        </Provider>
      );
    }
  }
};
