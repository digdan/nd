import React from 'react';
import { Container, Content, Text} from 'native-base';
import HeaderNav from '../components/header/header';

export default class SocketScreen extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Text>Socket Screen</Text>
        </Content>
      </Container>
    );
  }
}
