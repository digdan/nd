import React, { Component } from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import HomeScreen from './HomeScreen';
import SocketScreen from './SocketScreen';

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  Socket: {screen: SocketScreen}
}, {
  initialRouteName: 'Socket'
});

const Navigator = createAppContainer(MainNavigator);

class SetupScreens extends Component {
  constructor() {
    super();
    this.state = {
      setupComplete: false
    }
  }

  componentDidMount() {
    console.log('nav', MainNavigator
    console.log('this props', this.props)
  }

  render() {
    return (
      <Navigator/>
    );
  }
}

export default SetupScreens;
