import React from 'react';
import { Container, Content, Text} from 'native-base';
import HeaderNav from '../components/header/header';

export default class HomeScreen extends React.Component {
  render() {
    return (
      <Container>
        <HeaderNav
          menuNotif={0}
          groupNotif={28}
        />
        <Content>
          <Text>Testing A</Text>
          <Text>Testing B</Text>
        </Content>
      </Container>
    );
  }
}
