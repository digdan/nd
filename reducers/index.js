import { combineReducers } from 'redux';

export const RESET_STORE = 'RESET_STORE';

const base = (state={}, action) => {
  console.log('Action', action);
  return state;
}

const appReducer = () => {
  return combineReducers({
    base
  });
};

const rootReducer = () => {
  return (state={}, action) => {
    if (action.type === RESET_STORE) {
      state = {};
    }
    return appReducer()(state, action);
  };
};

export const reset = () => {
  return dispatch => {
    dispatch({
      type: RESET_STORE
    });
  };
};

export default rootReducer;
