import { Platform } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import devTools from 'remote-redux-devtools';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const enhancers = [
];
const initialState = {
  base: {
    one: 2
  }
};
const middleware = [
  thunk,
  sagaMiddleware
];

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  devTools({
    name: Platform.OS,
    hostname: 'localhost',
    port: 5678
  }),
  ...enhancers
);

if (typeof devToolsExtension === 'function') {
  enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
}

const store = createStore(
  rootReducer(),
  initialState,
  composedEnhancers
);

sagaMiddleware.run(rootSaga);
export default store;
