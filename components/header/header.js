import React, { Component } from 'react';
import { Container, Header, Left, Body, Right, Badge, Text, Button, Icon, Title, Subtitle } from 'native-base';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class HeaderNav extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='menu' />
              {this.props.menuNotif > 0 ? (
                <Badge primary style={{ position: 'absolute', opacity: 0.5, right:0 }}><Text>{this.props.menuNotif}</Text></Badge>
              ) : (null)}
            </Button>
          </Left>

          <Body>
            <Title>{this.props.title}</Title>
            <Subtitle>{this.props.subtitle}</Subtitle>
          </Body>

          <Right>
            <Button transparent>
              <Icon type='MaterialCommunityIcons' name='account-group'/>
              {this.props.groupNotif > 0 ? (
                <Badge primary style={{ position: 'absolute', opacity: 0.5, right:0 }}><Text>{this.props.groupNotif}</Text></Badge>
              ) : (null)}
            </Button>
          </Right>
        </Header>
      </Container>
    );
  }
}

HeaderNav.defaultProps = {
  title: 'Header Title',
  subtitle: 'Header Subtitle',
  groupNotif: 0,
  menuNotif: 0
};

function menuClick(dispatch) {
  dispatch({
    type:'test'
  });
}

const mapDispatchToProps = dispatch => bindActionCreators({
  menuClick: params => menuClick(params)
}, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(HeaderNav);
